use std::env;
use std::error;

extern crate helper;
extern crate reqwest;

macro_rules! env {
    ($e:expr) => {{
        env::var($e).unwrap_or_else(|err| {
            panic!("{} {}", $e, err);
        })
    }};
}

macro_rules! get {
    ($client:ident,$url:ident,$creds:ident) => {
        $client
            .get(&$url)
            .basic_auth(&$creds.0, Some(&$creds.1))
            .send()?
            .text()?;
    };
}

macro_rules! active_sprint_url {
    ($($t:tt)*) => {
        format!(
            "https://{jira_host}/rest/agile/latest/board/{board_id}/sprint?state=active",
            $($t)*)
    };
}

macro_rules! sprint_stories_url {
    ($($t:tt)*) => {
        format!(
            "https://{jira_host}/rest/agile/latest/board/{board_id}/sprint/{sprint}/issue",
            $($t)*)
    };
}

fn main() -> Result<(), Box<dyn error::Error>> {
    let host = env!("JIRA_HOST");
    let board_id = env!("JIRA_BOARD");
    let creds = helper::get_account("jira")?;

    let url = active_sprint_url!(jira_host = host, board_id = board_id);
    println!("{}", url);

    let client = reqwest::Client::new();
    let res = get!(client, url, creds);

    let sprint = helper::get_json(&res)?;
    let id = &sprint["values"][0]["id"];

    let url = sprint_stories_url!(jira_host = host, board_id = board_id, sprint = id);
    println!("{}", url);

    let res = get!(client, url, creds);
    let v = helper::get_json(&res)?;

    println!("{}", v["total"]);
    return Ok(());
}
