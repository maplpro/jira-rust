use std::process::Command;
use std::str;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let res = Command::new("security")
        .args(&["find-generic-password", "-g", "-s", "test_jira"])
        .arg("-g")
        .output()?;

    let out = res.stdout;
    let v = str::from_utf8(&out)?;

    println!("{}\n", v);

    let out = res.stderr;
    let v = str::from_utf8(&out)?;

    println!("{}\n", v);

    Ok(())
}
