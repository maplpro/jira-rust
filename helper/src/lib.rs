use serde_json::Value;
use std::process::Command;
use std::str;

#[allow(unused_macros)]
macro_rules! s {
    ($s:expr) => {
        String::from($s)
    };
}

#[test]
fn check_account() {
    let creds = get_account("test_jira").unwrap();
    assert_eq!(creds, (s!("next"), s!("carb472:crawl")));
}

#[test]
fn test_sprint_res() {
    let sprint = r#"
    {
        "maxResults":50,
        "startAt":0,
        "isLast":true,
        "values":[
            {"id":10785,
            "self":"https://host/rest/agile/1.0/sprint/10785",
            "state":"active",
            "name":"Sprint 95",
            "startDate":"2018-12-24T02:13:36.418-08:00",
            "endDate":"2018-12-29T02:13:00.000-08:00",
            "originBoardId":1952,
            "goal":""}]}
    "#;

    let v = get_json(sprint).unwrap();
    let obj = v.as_object().unwrap();

    let n = &obj["values"][0]["name"];
    assert_eq!(n, "Sprint 95");
}

pub fn get_json(s: &str) -> Result<Value, Box<dyn std::error::Error>> {
    let v: Value = serde_json::from_str(&s)?;
    Ok(v)
}

pub fn get_account(svc: &str) -> Result<(String, String), Box<dyn std::error::Error>> {
    let res = Command::new("security")
        .args(&["find-generic-password", "-g", "-s", svc])
        .arg("-g")
        .output()?;

    let get = |s: &str, t: &str| {
        let idx = s.find(t).unwrap();
        let s = &s[idx + t.len()..];

        let idx = s.find('"').unwrap();
        s!(&s[..idx])
    };

    let out = res.stdout;
    let usr = str::from_utf8(&out)?;
    let usr = get(usr, "acct\"<blob>=\"");

    let out = res.stderr;
    let pwd = str::from_utf8(&out)?;
    let pwd = get(pwd, "password: \"");

    Ok((s!(usr), s!(pwd)))
}
